var jade = require('gulp-jade');
var stylus = require('gulp-stylus');
var gulp = require('gulp');
var connect = require('gulp-connect');
var watch = require('gulp-watch');
//var debug = require('gulp-debug');

//Webサーバー
gulp.task('connect', function() {
  connect.server({
    root: 'build',//ルートディレクトリ
    livereload: true //ライブリロード
  });
});

gulp.task('css', function () {
  gulp.src('lp/*.styl')
    //.pipe(debug({verbose: true}))
    .pipe(stylus({}))
    .pipe(gulp.dest('build/assets'));
});

/*gulp.task('css2', function () {
  gulp.src('topic/*.sty*')
    //.pipe(debug({verbose: true}))
    .pipe(stylus({}))
    .pipe(gulp.dest('topic/'));
});s
*/

gulp.task('jade',function() {
  gulp.src('./lp/*.jade')
    //.pipe(debug({verbose: true}))
    .pipe(jade({}))
    .pipe(gulp.dest('./build/'));
});

gulp.task('images', function(){
  gulp.src('./lp/images/*')
    .pipe(gulp.dest('./build/assets'));
});

gulp.task('js', function(){
  gulp.src('./lp/lib/*')
    .pipe(gulp.dest('./build/lib/'));
});

//'html'に、htmlファイルをリロードする処理を登録
gulp.task('html', function () {
  gulp.src('./build/*.html')
    .pipe(connect.reload());
});

//監視：HTMLファイルが変更されたら'html'を実行
gulp.task('watch', function () {
  gulp.watch(['./build/*.html'], ['html']);
});

//デフォルトタスクに登録
gulp.task('default', ['css', 'jade', 'images', 'js','connect', 'watch']);
